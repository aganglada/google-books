import paramsService from 'services/params/params';

const { fetch } = window;
const baseUrl = `https://www.googleapis.com/books/v1/volumes`;
const limit = 20;
const params = {
    q: 'javascript',
    maxResults: limit,
    orderBy: 'newest'
};

const libraryService = () => {
    function requestBooks(context) {
        return fetch(`${baseUrl}?${paramsService.toQueryString(params)}`)
            .then(response => response.json())
            .then(({ items }) => context.books = items);
    }

    return {
        requestBooks
    };
};

export default libraryService();