import Component from 'components/component';
import book from 'components/book/book';

import libraryService from './library.service';

const classNames = [
    'u-max-width--900',
    'u-flex u-flex--grid',
    'u-padding',
    'u-margin--zero-auto'
];

class Library extends Component {

    onInit() {
        this.books = [];

        libraryService.requestBooks(this).then(() => {
            this.el.innerHTML = this.render();
        });
    }

    render() {
        return `
            <section class="${classNames.join(' ')}">${this.books.map(b => {
                return book(b);
            }).join('')}</section>
        `;
    }
}

export default Library;

