import Component from 'components/component';

class Header extends Component {
    render() {
        return `
            <h1 class="c-header__title">google books</h1>
        `;
    }
}

export default Header;