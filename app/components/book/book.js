import stringService from 'services/string/string';

export default (props) => {
    const { volumeInfo: book } = props;

    return `
        <article class="c-book">
            <header class="c-book__cover">
                <img class="c-book__cover-img" src="${book.imageLinks.thumbnail}" alt="${book.title}" title="${book.title}">
            </header>
            <section class="u-padding">
                <h3 class="c-book__title">${book.title}</h3>
                <p class="u-color--grey-lighter">
                    ${book.description ? `${
                        stringService.substring(book.description, 200)
                    }... <a target="_blank" href="${book.infoLink}">read more</a>` : 'No description available'}
                    </p>
            </section>
        </article>
    `;
}