class Component {

    constructor(el, props) {
        this.el = el;

        if (this.onInit) this.onInit.call(this);

        this.init(el, props);
    }

    init(el, props) {

        if (!this.render) {
            throw `Component ${this.constructor.name} must have a render method`;
        }

        el.innerHTML = this.render(props);
    }
}

export default Component;