const euc = encodeURIComponent;

const paramsService = () => {
    function toQueryString(params) {
        return Object.keys(params)
            .map(k => euc(k) + '=' + euc(params[k]))
            .join('&');
    }

    return {
        toQueryString
    };
};

export default paramsService();