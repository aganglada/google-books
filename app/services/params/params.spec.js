import paramsService from './params';

describe('Service:Params', () => {

    it('should be an object', () => {
        expect(typeof paramsService === 'object').toBeTruthy()
    });

    it('should have a function called toQueryString', () => {
        expect(typeof paramsService.toQueryString).toBeDefined()
    });

    describe('toQueryString', () => {

        let result;
        const params = {
            q: 'javascript',
            maxResults: 20,
            orderBy: 'newest'
        };

        beforeEach(() => {
            result = paramsService.toQueryString(params)
        });

        it('should be a function', () => {
            expect(typeof paramsService.toQueryString === 'function').toBeTruthy();
        });

        it('should receive an object and return a string', () => {
            expect(typeof result === 'string').toBeTruthy();
        });

        it('should have all the properties from the object in the string', () => {
            Object.keys(params).map(param => {
                expect(result.indexOf(param) > -1).toBeTruthy();
            })
        });
    })

});