const stringService = () => {
    function substring(string, options) {
        let o = {};

        if (typeof options !== 'object') {
            o.start = 0;
            o.stop = options;
        }

        return string.substring(o.start, o.stop);
    }

    return {
        substring
    };
};

export default stringService();