import stringService from './string';

describe('Service:String', () => {

    it('should be an object', () => {
        expect(typeof stringService === 'object').toBeTruthy()
    });

    it('should have a function called substring', () => {
        expect(typeof stringService.substring).toBeDefined()
    });

    describe('substring', () => {
        let result;
        const stop = 200;
        const text = `
            JavaScript es un lenguaje de programación de computadoras dinámico utilizado habitualmente en navegadores web para controlar el comportamiento de páginas web e interactuar con los usuarios. Permite comunicación asincrónica y puede actualizar partes de una página web o incluso reemplazar completamente su contenido. Verás que JavaScript es utilizado para mostrar información de fecha y hora, ejecutar animaciones en un sitio web, validar formularios, sugerir resultados mientras el usuario escribe en un cuadro de búsqueda y más.
            JavaScript está siendo usado cada vez más...
            A pesar de que JavaScript es, por mucho, el lenguaje de programación de cliente más popular actualmente en uso, puede ser usado –y, de hecho, lo es– también en el servidor. Node.js, Meteor, Wakanda, CouchDB y MongoDB son sólo unos pocos ejemplos de lugares donde encontrarás y serás capaz de usar JavaScript en el servidor. El tiempo que inviertas en aprender JavaScript puede ser doblemente rendidor, ya que JavaScript sigue expandiéndose por cada vez más áreas de la computación.
            Aprende los fundamentos del lenguaje de programación JavaScript
            Ya sea que planees usar JavaScript en el lado del cliente en un navegador web, en el lado del servidor, o ambos, necesitas aprender los fundamentos del lenguaje. Esto es lo que te proporcionará este libro. Cuando termines de leerlo, te sentirás cómodo y confiado programando en Lenguaje JavaScript.
            Estas son algunas cosas que aprenderás cuando leas este libro:
            Dónde puede usarse JavaScript
            Cómo configurar tu computadora para programar en JavaScript cómoda y fácilmente
            Qué herramientas debes tener cuando programes en JavaScript
            Los fundamentos del HTML...
            Qué son las variables y cómo usarlas
            Cómo manejar números y realizar operaciones matemáticas
            Cómo y cuándo usar condicionales
            Qué funciones hay, por qué son tan útiles y
        `;

        beforeEach(() => {
            result = stringService.substring(text, stop);
        });

        it(`should return a string no longer than ${stop}`, () => {
            expect(result.length > stop).toBeFalsy();
        });

        it(`should return a string no shorter than ${stop}`, () => {
            expect(result.length < stop).toBeFalsy();
        });

        it(`should return a string exactly equal length than ${stop}`, () => {
            expect(result.length === stop).toBeTruthy();
        });
    })
});