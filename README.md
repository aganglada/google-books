# aganglada/google-books

## Technologies

* webpack
* es6 (VanillaJS)
* sass
* postcss
* karma
* jasmine

## Running it locally

You will need `node` and `npm` installed before.

1. Clone the project: `git clone git@bitbucket.org:aganglada/google-books.git`
2. Go inside the project folder: `cd google-books`
3. Install dependencies: `npm install`
4. Serve it to your preferred browser: `npm run dev`
6. Access to `http://localhost:8080`

To run the test, use `npm run test:watch`
