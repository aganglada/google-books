'use strict';

module.exports = function (test) {
    var obj = {
        style: './.tmp/app.css',
        app: './app/app.js'
    };

    return test ? {} : obj;
};
